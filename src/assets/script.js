window.onload = function() {
    setInterval(function() {
        var date = new Date();
        var displayDate = date.toLocaleDateString();
        var displayTime = date.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});

        document.getElementById('datetime').innerHTML = displayTime;
    }, 1000); // 1000 milliseconds = 1 second
}
